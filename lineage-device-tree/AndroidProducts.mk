#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus5.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus5-user \
    lineage_OnePlus5-userdebug \
    lineage_OnePlus5-eng
