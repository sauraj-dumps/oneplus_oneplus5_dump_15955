#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_OnePlus5.mk

COMMON_LUNCH_CHOICES := \
    omni_OnePlus5-user \
    omni_OnePlus5-userdebug \
    omni_OnePlus5-eng
